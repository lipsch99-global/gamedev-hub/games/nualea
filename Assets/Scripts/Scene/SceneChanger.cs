﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneChanger : MonoBehaviour
{
    public Image BlendImage;
    public float BlendSpeed = 1f;

    private SoundManager soundManager;

    void Start()
    {
        if (FindObjectsOfType<SceneChanger>().Length != 1)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
        soundManager = FindObjectOfType<SoundManager>();

    }

    public void GoToMenuScene()
    {
        soundManager.LowerBackgroundMusic();
        StartCoroutine(ChangeScene(0));

    }
    public void GoToMainScene()
    {
        soundManager.RaiseBackgroundMusic();
        StartCoroutine(ChangeScene(1));
    }
    public void GoToCharacterSelection()
    {
        soundManager.LowerBackgroundMusic();
        StartCoroutine(ChangeScene(2));
    }

    private IEnumerator ChangeScene(int index)
    {
        BlendImage.fillAmount = 0;
        while (BlendImage.fillAmount < 1)
        {
            BlendImage.fillAmount += BlendSpeed * Time.deltaTime;
            yield return null;
        }

        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(index);
        while (asyncOperation.isDone == false)
        {
            yield return null;
        }


        BlendImage.fillAmount = 1;
        while (BlendImage.fillAmount > 0)
        {
            BlendImage.fillAmount -= BlendSpeed * Time.deltaTime;
            yield return null;
        }

        SceneChanged(index);
    }
    private void SceneChanged(int index)
    {
        if (index == 0)
        {
            GameObject startbutton = GameObject.Find("Start Button");
            Button startButtonScript = startbutton.GetComponent<Button>();
            if (startButtonScript != null)
            {
                startButtonScript.onClick.AddListener(new UnityEngine.Events.UnityAction(GoToCharacterSelection));
            }
        }

    }
}
