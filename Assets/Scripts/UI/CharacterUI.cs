﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterUI : MonoBehaviour
{
    public CharacterAndPrice Character;
    public Transform ShowCaseTransform;
    public GameObject GroundTilePrefab;
    public GameObject PlayerPrefab;
    public GameObject WalkableTilePrefab;

    public GameObject SelectedSign;
    public GameObject BuySign;

    public Text BuyPrice;

    public System.Action<CharacterUI> Clicked;


    public int TileSize { get; private set; }

    // Use this for initialization
    void Start()
    {
    }

    public void ShowEverything(float space)
    {
        TileSize = Mathf.RoundToInt(space * 0.85f / 7f);

        BuyPrice.text = Character.price.ToString();
        for (int x = -3; x <= 3; x++)
        {
            for (int y = -3; y <= 3; y++)
            {
                Vector2Int pos = new Vector2Int(x, y);

                GameObject groundTile = Instantiate(GroundTilePrefab, ShowCaseTransform);
                groundTile.transform.localPosition = new Vector3(x * TileSize, y * TileSize, 0);
                groundTile.GetComponent<RectTransform>().sizeDelta = new Vector2(TileSize, TileSize);

                if (Character.character.MovePattern.Contains(pos))
                {
                    GameObject walkableTile = Instantiate(WalkableTilePrefab, groundTile.transform);
                    walkableTile.GetComponent<CharacterUIItem>().Setup(this);
                }
                if (pos == Vector2Int.zero)
                {
                    GameObject player = Instantiate(PlayerPrefab, groundTile.transform);
                    player.GetComponent<CharacterUIItem>().Setup(this);
                }
            }
        }
    }

    public void CharacterClicked()
    {
        Clicked(this);
    }

    public void SetSelectedStatus(bool status)
    {
        SelectedSign.SetActive(status);
    }

    public void SetBuyStatus(bool status)
    {
        BuySign.SetActive(status);
    }
}
