﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CharacterUIItem : MonoBehaviour
{
    public void Setup(CharacterUI parent)
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        rectTransform.sizeDelta = new Vector2(parent.TileSize, parent.TileSize);
    }
}
