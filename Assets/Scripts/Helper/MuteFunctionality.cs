﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuteFunctionality : MonoBehaviour
{
    public Sprite musicMuted;
    public Sprite musicUnMuted;
    public Sprite soundMuted;
    public Sprite soundUnMuted;

    public Image musicImage;
    public Image soundImage;

    private SoundManager _soundManager;
    private SoundManager soundManager
    {
        get
        {
            if (_soundManager == null)
            {
                _soundManager = FindObjectOfType<SoundManager>();
            }
            return _soundManager;
        }
    }


    void Start()
    {
        musicImage.sprite = soundManager.IsMusicMuted ? musicMuted : musicUnMuted;
        soundImage.sprite = soundManager.IsSoundMuted ? soundMuted : soundUnMuted;
    }

    public void ChangeMusicMuteState()
    {
        soundManager.SetMusicMuteState(!soundManager.IsMusicMuted);
        musicImage.sprite = soundManager.IsMusicMuted ? musicMuted : musicUnMuted;
    }

    public void ChangeSoundMuteState()
    {
        soundManager.SetSoundMuteState(!soundManager.IsSoundMuted);
        soundImage.sprite = soundManager.IsSoundMuted ? soundMuted : soundUnMuted;
    }
}
