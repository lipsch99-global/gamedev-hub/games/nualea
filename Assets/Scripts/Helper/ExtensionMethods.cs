﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods
{
    public static Vector2Int DivideFloored(this Vector2Int vector, float divisor)
    {
        return new Vector2Int(Mathf.FloorToInt(vector.x / divisor), Mathf.FloorToInt(vector.y / divisor));
    }

    public static Vector2Int DivideCeiled(this Vector2Int vector, float divisor)
    {
        return new Vector2Int(Mathf.CeilToInt(vector.x / divisor), Mathf.CeilToInt(vector.y / divisor));
    }
}
