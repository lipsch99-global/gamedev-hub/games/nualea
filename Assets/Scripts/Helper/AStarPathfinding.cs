﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public abstract class AStarPathfinding
{

    public static List<Vector2Int> Solve(Vector2Int start, Vector2Int goal, Map map, Character character)
    {
        // The set of nodes already evaluated
        List<Vector2Int> closedSet = new List<Vector2Int>();


        // The set of currently discovered nodes that are not evaluated yet.
        // Initially, only the start node is known.
        List<Vector2Int> openSet = new List<Vector2Int>();
        openSet.Add(start);

        // For each node, which node it can most efficiently be reached from.
        // If a node can be reached from many nodes, cameFrom will eventually contain the
        // most efficient previous step.
        Dictionary<Vector2Int, Vector2Int> cameFrom = new Dictionary<Vector2Int, Vector2Int>(); //not zero..

        // For each node, the cost of getting from the start node to that node.
        Dictionary<Vector2Int, float> gScore = new Dictionary<Vector2Int, float>(); //default value or infinity

        // The cost of going from start to start is zero.
        gScore[start] = 0;

        // For each node, the total cost of getting from the start node to the goal
        // by passing by that node. That value is partly known, partly heuristic.
        Dictionary<Vector2Int, float> fScore = new Dictionary<Vector2Int, float>(); //default value of Infinity

        // For the first node, that value is completely heuristic.
        fScore[start] = HeuristicCostEstimate(start, goal, character);


        while (openSet.Count > 0)
        {

            float smallestFScore = float.MaxValue;
            Vector2Int current = openSet.First();

            //the node in openSet having the lowest fScore[] value
            foreach (var item in openSet)
            {
                float currentFScore = float.MaxValue;
                if (fScore.ContainsKey(item))
                {
                    currentFScore = fScore[item];
                }

                if (currentFScore < smallestFScore)
                {
                    current = item;
                    smallestFScore = fScore[item];
                }
            }


            if (current.x == goal.x && current.y == goal.y)
            {
                return ReconstructPath(cameFrom, current);
            }

            openSet.Remove(current);
            closedSet.Add(current);



            foreach (var moveTile in character.MovePattern)
            {
                Vector2Int neighbour = moveTile + current;
                MapTile mapTile = map.GetMapTileByPosition(neighbour);
                if (closedSet.Any(thisTile => thisTile.x == neighbour.x && thisTile.y == neighbour.y))
                {
                    continue; // Ignore the neighbour which is already evaluated.
                }
                else if (mapTile == null)
                {
                    continue; //Ignore the neighbour if the map tile doesn't exist.
                }
                else if (mapTile != null && mapTile.IsPassable == false)
                {
                    continue; // Ignore the neighbour because it's not passable
                }
                else if (mapTile.EntityOnTile != null && mapTile.EntityOnTile is Player == false)
                {
                    continue; //if there is a entity on that tile that is not the player
                }
                else if (openSet.Any(thisTile => thisTile.x == neighbour.x && thisTile.y == neighbour.y) == false && closedSet.Any(thisTile => thisTile.x == neighbour.x && thisTile.y == neighbour.y) == false) // Discover a new node
                {
                    openSet.Add(neighbour);
                }

                // The distance from start to a neighbor
                //the "dist_between" function may vary as per the solution requirements.
                float currentGScore = float.MaxValue;
                if (gScore.ContainsKey(current))
                {
                    currentGScore = gScore[current];
                }

                float tentative_gScore = currentGScore + Vector2Int.Distance(current, neighbour);

                float neighbourGScore = float.MaxValue;
                if (gScore.ContainsKey(neighbour))
                {
                    neighbourGScore = gScore[neighbour];
                }

                if (tentative_gScore >= neighbourGScore)
                {
                    continue; // This is not a better path.
                }

                // This path is the best until now. Record it!
                cameFrom[neighbour] = current;
                gScore[neighbour] = tentative_gScore;
                fScore[neighbour] = gScore[neighbour] + HeuristicCostEstimate(neighbour, goal, character);
            }

        }
        return new List<Vector2Int> { start };
        //throw new System.Exception("A* Algorithm didn't work!");
    }

    private static float HeuristicCostEstimate(Vector2Int start, Vector2Int goal, Character character)
    {
        return Vector2Int.Distance(start, goal) / character.MaxMovement();
    }

    private static List<Vector2Int> ReconstructPath(Dictionary<Vector2Int, Vector2Int> cameFrom, Vector2Int current)
    {
        List<Vector2Int> totalPath = new List<Vector2Int>();
        Vector2Int localCurrent = current;
        totalPath.Add(current);

        while (cameFrom.Keys.Contains(localCurrent))
        {
            localCurrent = cameFrom[localCurrent];
            totalPath.Add(localCurrent);
        }
        totalPath.Reverse();
        totalPath.RemoveAt(0);
        return totalPath;
    }
}
