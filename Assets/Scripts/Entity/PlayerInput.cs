﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    private RoundManager roundManager;
    private Player player;
    // Use this for initialization
    void Start()
    {
        roundManager = FindObjectOfType<RoundManager>();
        player = GetComponent<Player>();
        Input.simulateMouseWithTouches = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (roundManager != null && roundManager.WhosTurn == RoundManager.Turn.Player)
        {
            Vector2 clickedPos = Vector2.zero;
            //Touch Pos
            if (Input.touchCount >= 1 && Input.touches[0].phase == TouchPhase.Ended)
            {
                clickedPos = Input.touches[0].position;

            }
            //Mouse click Pos
            else if (Input.GetMouseButtonDown(0))
            {
                clickedPos = Input.mousePosition;
            }
            else
            {
                return;
            }

            player.PlayerClicked(clickedPos);
        }
    }
}
