﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EntityMovement))]
public abstract class Entity : MonoBehaviour , IInitialisable
{
    [HideInInspector]
    public Vector2Int Position;
    public Character EntitiyCharacter;

    [HideInInspector]
    public Map map;
    [HideInInspector]
    public EntityMovement entityMovement;

    public GameObject MovePatternEffectPrefab;

    

    public virtual void Initialise()
    {
        entityMovement = GetComponent<EntityMovement>();
        map = FindObjectOfType<Map>();
        AlignPosition();
        entityMovement.Initialise();
    }

    public void AlignPosition()
    {
        MapTile tileToStandOn = map.GetMapTileByPosition(Position);

        transform.position = tileToStandOn.transform.position;
    }

    public bool IsPositionInWalkPattern(Vector2Int pos)
    {
        foreach (var offset in EntitiyCharacter.MovePattern)
        {
            if (offset + Position == pos)
            {
                return true;
            }
        }
        return false;
    }

    public void Kill(Entity entity)
    {
        if (entity is Player)
        {
            FindObjectOfType<RoundManager>().PlayerDied();
        }
        else if (entity is Enemy)
        {
            ((Enemy)entity).Died((Enemy)entity,this);
            Destroy(entity.gameObject);
        }
    }
}
