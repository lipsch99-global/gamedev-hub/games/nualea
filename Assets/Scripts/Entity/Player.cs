﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Player : Entity
{
    [HideInInspector]
    public RoundManager roundManager;

    private Vector2Int? lastShownWalkPattern;

    public void PlayerClicked(Vector2 clickedPos)
    {
        //Where did the Player click
        Vector2 worldPoint = Camera.main.ScreenToWorldPoint(clickedPos);
        RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero);
        if (hit.collider != null)
        {
            MapTile tile = hit.transform.GetComponent<MapTile>();
            if (tile != null)
            {
                if (tile.EntityOnTile != null && tile.EntityOnTile is Enemy)
                {
                    if (lastShownWalkPattern == null || tile.Position != lastShownWalkPattern)
                    {
                        map.ClearAllTileEffects();
                        entityMovement.ShowMovePattern();
                        tile.EntityOnTile.entityMovement.ShowMovePattern();
                        lastShownWalkPattern = tile.Position;
                        return;
                    }
                }

                if (IsPositionInWalkPattern(tile.Position))
                {
                    Vector2Int newPos = tile.Position - Position;
                    entityMovement.MoveOffset(newPos);
                    map.ReloadMap();
                    entityMovement.ShowMovePattern();

                    lastShownWalkPattern = null;
                    return;
                }
            }
        }

    }

    public override void Initialise()
    {
        base.Initialise();
        roundManager = FindObjectOfType<RoundManager>();
        entityMovement.ShowMovePattern();
    }
}
