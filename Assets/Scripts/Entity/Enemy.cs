﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Entity
{
    public enum SpawnRange { Low, Mid, High }
    public SpawnRange minSpawnRange;
    public SpawnRange maxSpawnRange;

    public int CoinDropAmount = 1;

    public System.Action<Enemy, Entity> Died;


    private Player player;

    public override void Initialise()
    {
        base.Initialise();
        player = FindObjectOfType<Player>();
    }

    public void DoTurn()
    {
        List<Vector2Int> path = AStarPathfinding.Solve(Position, player.Position, map, EntitiyCharacter);
        entityMovement.MoveOffset(path[0] - Position);
    }


}
