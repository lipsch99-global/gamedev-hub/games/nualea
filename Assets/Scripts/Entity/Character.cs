﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "NewCharacter", menuName = "Character")]
public class Character : ScriptableObject
{
    public List<Vector2Int> MovePattern;


    public float MaxMovement()
    {
        Vector2Int biggestDistance = new Vector2Int(int.MinValue,int.MinValue);
        foreach (var movePos in MovePattern)
        {
            if (movePos.magnitude > biggestDistance.magnitude)
            {
                biggestDistance = movePos;
            }
        }
        return biggestDistance.magnitude;
    }
    
}
