﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinPopup : MonoBehaviour
{
    public Text coinAmountText;
    public float Duration;
    public float Speed;

    public void StartMoving()
    {
        StartCoroutine(Moving());
    }

    private IEnumerator Moving()
    {
        float start = Time.time;

        while (start+Duration >= Time.time)
        {
            transform.Translate(Vector3.up * Speed);
            yield return null;
        }

        Destroy(gameObject);
    }

    
}
