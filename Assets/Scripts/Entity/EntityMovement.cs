﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Entity))]
public class EntityMovement : MonoBehaviour, IInitialisable
{
    public float MoveTime = 0.5f;
    public AnimationCurve jumpCurve;
    public float jumpHeight = 1f;

    private Entity entity;
    private CameraController camController;


    /// <summary>
    /// Moves the Entity to a new MapTile
    /// </summary>
    /// <param name="newPosition">The absolute Position</param>
    public void Move(Vector2Int newPosition)
    {
        MapTile tileEntityIsOn = entity.map.GetMapTileByPosition(entity.Position);

        //When entity is outside of playZone -> kill it
        //if (tileEntityIsOn == null)
        //{
        //    entity.Kill(entity);
        //    return;
        //}

        MapTile tileToMoveOn = entity.map.GetMapTileByPosition(newPosition);
        if (tileToMoveOn != null && tileToMoveOn.IsPassable && entity.IsPositionInWalkPattern(newPosition))
        {
            bool killedEnemy = false;
            if (tileToMoveOn.EntityOnTile != null && tileToMoveOn.EntityOnTile != entity)
            {
                entity.Kill(tileToMoveOn.EntityOnTile);
                tileToMoveOn.UnsetEntity();
                killedEnemy = true;
            }

            entity.Position = newPosition;
            tileEntityIsOn.UnsetEntity();
            tileToMoveOn.SetEntity(entity);

            //start moving the sprite
            StartCoroutine(MoveSmooth(transform.position, tileToMoveOn.transform.position, killedEnemy));
        }
    }

    public void MoveOffset(Vector2Int offset)
    {
        Move(entity.Position + offset);
    }

    private IEnumerator MoveSmooth(Vector2 from, Vector2 to, bool killedEnemy)
    {
        if (entity is Player)
        {
            Player player = (Player)entity;
            player.roundManager.PlayerStartsMove();
        }

        Vector2 dir =  (to - from);
        float distance = Vector2.Distance(from, to);
        float distanceTraveled = 0;

        float lastJumpAdd = 0;
        while (distanceTraveled <= distance)
        {
            Vector2 movement = dir * Time.deltaTime * (1f/MoveTime);
            if (distanceTraveled > distance / 10f * 9f)
            {
                movement = movement / 3f;
            }

            float jumpAdd = jumpCurve.Evaluate(distanceTraveled / distance) * jumpHeight - lastJumpAdd;
            lastJumpAdd = jumpAdd;

            transform.position = transform.position + new Vector3(movement.x, movement.y + jumpAdd, 0);
            distanceTraveled += movement.magnitude;
            yield return null;
        }
        transform.position = to;

        if (entity is Player)
        {
            Player player = (Player)entity;
            player.roundManager.PlayerDidTurn();

            if (killedEnemy)
            {
                camController.CameraShake(4, 0.075f, 3);
            }
        }
    }
    public void ShowMovePattern()
    {
        foreach (var walkOffset in entity.EntitiyCharacter.MovePattern)
        {
            MapTile tile = entity.map.GetMapTileByPosition(entity.Position + walkOffset);
            if (tile != null && tile.IsPassable)
            {
                tile.SetEffect(entity.MovePatternEffectPrefab);
            }
        }

    }

    public void Initialise()
    {
        entity = GetComponent<Entity>();
        camController = FindObjectOfType<CameraController>();
    }
}
