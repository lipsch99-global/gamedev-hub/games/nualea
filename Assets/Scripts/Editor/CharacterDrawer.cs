﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

[CustomEditor(typeof(Character))]
public class CharacterDrawer : Editor
{
    public Texture2D walkable;
    public Texture2D notWalkable;
    private Dictionary<Rect, Vector2Int> tiles;

    public override void OnInspectorGUI()
    {
        Character characterTarget = (Character)target;

        Event guiEvent = Event.current;

        if (guiEvent.type == EventType.Repaint)
        {
            Draw();
        }
        else if(guiEvent.type == EventType.MouseDown && guiEvent.button == 0)
        {
            foreach (var tile in tiles.Keys)
            {
                if (tile.Contains(guiEvent.mousePosition))
                {
                    if (characterTarget.MovePattern.Contains(tiles[tile]))
                    {
                        characterTarget.MovePattern.Remove(tiles[tile]);
                    }
                    else
                    {
                        characterTarget.MovePattern.Add(tiles[tile]);
                    }
                    EditorUtility.SetDirty(target);
                    break;
                }
            }
        }


    }

    private void Draw()
    {
        Character characterTarget = (Character)target;
        tiles = new Dictionary<Rect, Vector2Int>();


        //base.OnInspectorGUI();
        float border = Screen.width / 10f;
        Vector2Int fieldSize = new Vector2Int(7, 7);
        Vector2Int startPos = Vector2Int.zero - new Vector2Int(fieldSize.x / 2, fieldSize.y / 2);
        Vector2Int size = new Vector2Int(40, 40);
        Vector2 padding = new Vector2(5, 5);

        float maxSpriteSize = (Screen.width - (border * 2) - padding.x * fieldSize.x) / fieldSize.x;
            if (maxSpriteSize < size.x)
        {
            size = new Vector2Int(Mathf.FloorToInt(maxSpriteSize), Mathf.FloorToInt(maxSpriteSize));
        }


        Vector2 pos = new Vector2(border, 75);
        for (int xPos = startPos.x; xPos < startPos.x + fieldSize.x; xPos++)
        {
            pos.y = 75;
            for (int yPos = startPos.y; yPos < startPos.y + fieldSize.y; yPos++)
            {
                Texture2D texToUse = walkable;
                if (characterTarget.MovePattern.Contains(new Vector2Int(xPos, yPos)))
                {
                    texToUse = notWalkable;
                }
                Rect tileRect = new Rect(pos, size);
                GUI.DrawTexture(tileRect, texToUse, ScaleMode.StretchToFill, true);
                tiles[tileRect] = new Vector2Int(xPos, yPos);
                pos.y += padding.y + size.y;
            }
            pos.x += padding.x + size.x;
        }
    }
}
