﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Map : MonoBehaviour
{
    public TileSpawnRate[] TilePrefabs;
    public float SpacePerTile;

    [Range(0.0001f, 0.9999f)]
    public float perlinScale;

    public Vector2Int Size;

    private MapTile[,] MapTiles;

    private float MapSeed;

    private Player player;
    private EnemyManager enemyManager;


    void Awake()
    {
        TilePrefabs = TilePrefabs.OrderBy(x => x.SpawnOccurence).ToArray();
        MapSeed = Random.Range(0, 10);
        player = FindObjectOfType<Player>();
        enemyManager = FindObjectOfType<EnemyManager>();
    }

    public void ReloadMap()
    {
        ClearAllTileEffects();

        List<Entity> entities = new List<Entity>();
        entities.Add(player);

        if (enemyManager != null && enemyManager.Enemies != null)
        {
            foreach (var enemy in enemyManager.Enemies)
            {
                entities.Add(enemy);
            }
        }

        if (MapTiles != null)
        {
            for (int x = 0; x < Size.x; x++)
            {
                for (int y = 0; y < Size.y; y++)
                {
                    Destroy(MapTiles[x, y].gameObject);
                }
            }
        }

        MapTiles = new MapTile[Size.x, Size.y];

        List<Entity> unsetEntities = entities.ToList();
        for (int x = 0; x < Size.x; x++)
        {
            for (int y = 0; y < Size.y; y++)
            {
                //Vector2Int offset = new Vector2Int(Mathf.FloorToInt(Size.x / 2f), Mathf.FloorToInt(Size.y / 2f)) + playerPos;
                Vector2Int absoluteTilePos = Vector2Int.zero - Size.DivideFloored(2) + player.Position + new Vector2Int(x, y);

                MapTile tile = GetNewTileWithPosition(absoluteTilePos);
                tile.transform.position = new Vector3(absoluteTilePos.x * SpacePerTile, absoluteTilePos.y * SpacePerTile, 0);
                MapTiles[x, y] = tile;

                foreach (var entity in entities)
                {
                    if (entity.Position.x == absoluteTilePos.x && entity.Position.y == absoluteTilePos.y)
                    {
                        tile.SetEntity(entity);
                        unsetEntities.Remove(entity);
                        break;
                    }
                }
            }
        }
        for (int i = 0; i < unsetEntities.Count; i++)
        {
            unsetEntities[i].Kill(unsetEntities[i]);
        }
    }

    /// <summary>
    /// Creates a new MapTile at the given Position
    /// </summary>
    /// <param name="position">The absolute Position of the Tile</param>
    private MapTile GetNewTileWithPosition(Vector2Int position)
    {
        float perlinNoise = Mathf.PerlinNoise((position.x + MapSeed) * perlinScale, (position.y + MapSeed) * perlinScale);

        GameObject TileToUse = TilePrefabs[0].TilePrefab;

        foreach (var tile in TilePrefabs)
        {
            if (position == Vector2Int.zero && tile.TilePrefab.GetComponent<MapTile>().IsPassable == false)
            {
                continue;
            }
            if (tile.SpawnOccurence >= perlinNoise)
            {
                TileToUse = tile.TilePrefab;
                break;
            }
        }
        MapTile newMapTile = Instantiate(TileToUse, transform).GetComponent<MapTile>();
        newMapTile.Position = new Vector2Int(position.x, position.y);
        newMapTile.transform.name = "X" + position.x + "_Y" + position.y + "_" + TileToUse.name;
        return newMapTile;
    }

    /// <summary>
    /// Returns the MapTile at the given Position
    /// </summary>
    /// <param name="position">The absolute Position of the Tile</param>
    public MapTile GetMapTileByPosition(Vector2Int position)
    {
        Vector2Int arrayPos = RealPosToArrayPos(position);
        if (arrayPos.x >= 0 && arrayPos.x < MapTiles.GetLength(0))
        {
            if (arrayPos.y >= 0 && arrayPos.y < MapTiles.GetLength(1))
            {
                return MapTiles[arrayPos.x, arrayPos.y];
            }
        }
        return null;
    }

    public void ClearAllTileEffects()
    {
        if (MapTiles != null && MapTiles.Length > 0)
        {
            foreach (var tile in MapTiles)
            {
                tile.RemoveEffect();
            }
        }
    }

    private Vector2Int RealPosToArrayPos(Vector2Int realPos)
    {
        Vector2Int offset = realPos - player.Position;
        return Size.DivideFloored(2) + offset;
    }
    private Vector2Int ArrayPosToRealPos(Vector2Int arrayPos)
    {
        Vector2Int offset = arrayPos - Size.DivideFloored(2);
        return player.Position + offset;
    }
}

[System.Serializable]
public struct TileSpawnRate
{
    public GameObject TilePrefab;

    [Range(0, 1)]
    public float SpawnOccurence;
}
