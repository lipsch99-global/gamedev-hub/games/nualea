﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapTile : MonoBehaviour
{
    public bool IsPassable;
    public Entity EntityOnTile { get; private set; }
    public TileEffect Effect { get; private set; }

    [HideInInspector]
    public Vector2Int Position;


    public void SetEffect(GameObject prefab)
    {
        RemoveEffect();
        Effect = Instantiate(prefab, transform).GetComponent<TileEffect>();
    }

    public void RemoveEffect()
    {
        if (Effect != null)
        {
            Destroy(Effect.gameObject);
        }
        Effect = null;
    }

    public void SetEntity(Entity entity)
    {
        EntityOnTile = entity;
    }

    public void UnsetEntity()
    {
        EntityOnTile = null;
    }
}
