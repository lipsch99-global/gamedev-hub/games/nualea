﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyManager : MonoBehaviour, IInitialisable
{
    public GameObject[] EnemyPrefabs;

    public List<Enemy> Enemies { get; private set; }

    public int EnemyAmount = 5;
    public float MinDistanceToPlayer = 6f;

    public int[] MoveMinRanges = new int[] { 0, 15, 30 };

    public GameObject CoinPopupPrefab;
    public Transform CoinPopupParent;

    private Map map;
    private RoundManager roundManager;
    private Player player;

    public void Initialise()
    {
        map = FindObjectOfType<Map>();
        roundManager = FindObjectOfType<RoundManager>();
        Enemies = new List<Enemy>();

        player = FindObjectOfType<Player>();
    }

    public void NewTurnStarted()
    {
        RespawnEnemies();

        Enemy[] localEnemies = Enemies.ToArray();
        for (int i = 0; i < localEnemies.Length; i++)
        {
            localEnemies[i].DoTurn();
        }
    }

    public void RespawnEnemies()
    {
        while (Enemies.Count < EnemyAmount)
        {
            SpawnRandomEnemy();
        }
    }

    private void SpawnRandomEnemy()
    {
        int difficulty = Mathf.FloorToInt(Mathf.Abs(player.Position.magnitude));

        List<GameObject> possibleEnemySpawns = new List<GameObject>();
        foreach (var enemyPrefab in EnemyPrefabs)
        {
            Enemy enemy = enemyPrefab.GetComponent<Enemy>();
            if (MoveMinRanges[(int)enemy.minSpawnRange] <= difficulty || enemy.minSpawnRange == Enemy.SpawnRange.Low)
            {
                if (MoveMinRanges[(int)enemy.maxSpawnRange] >= difficulty || enemy.maxSpawnRange == Enemy.SpawnRange.High)
                {
                    possibleEnemySpawns.Add(enemyPrefab);
                }
            }
        }

        GameObject randomEnemyPrefab = possibleEnemySpawns[UnityEngine.Random.Range(0, possibleEnemySpawns.Count)];

        MapTile randomTile;
        Vector2Int randomPos = Vector2Int.zero;
        do
        {
            Vector2Int halfMapSize = map.Size.DivideFloored(2);
            int x = UnityEngine.Random.Range(halfMapSize.x * -1 + 1, halfMapSize.y - 1);
            int y = UnityEngine.Random.Range(halfMapSize.x * -1 + 1, halfMapSize.y - 1);
            randomPos = new Vector2Int(x, y) + player.Position;
            randomTile = map.GetMapTileByPosition(randomPos);
        }
        while (randomTile.IsPassable == false || randomTile.EntityOnTile != null ||
            Vector2.Distance(player.transform.position, randomTile.transform.position) < MinDistanceToPlayer);

        Enemy newEnemy = Instantiate(randomEnemyPrefab).GetComponent<Enemy>();
        Enemies.Add(newEnemy);
        newEnemy.Died += EnemyDied;
        newEnemy.Position = randomPos;
        newEnemy.Initialise();
        randomTile.SetEntity(newEnemy);
    }

    private void EnemyDied(Enemy enemy, Entity killer)
    {
        if (killer is Player)
        {
            SpawnCoinPopUp(enemy);
            roundManager.EnemyKilled(enemy.CoinDropAmount);
        }
        Enemies.Remove(enemy);
    }

    private void SpawnCoinPopUp(Enemy enemy)
    {
        CoinPopup coin = Instantiate(CoinPopupPrefab, CoinPopupParent).GetComponent<CoinPopup>();
        coin.transform.position = enemy.transform.position;
        coin.coinAmountText.text = "+" + enemy.CoinDropAmount;
        coin.StartMoving();
    }


}
