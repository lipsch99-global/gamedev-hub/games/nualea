﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Advertisements;

public class GameManager : MonoBehaviour
{
    public int Points
    {
        get
        {
            return PlayerPrefs.GetInt("Points");
        }
        private set
        {
            PlayerPrefs.SetInt("Points", value);
        }
    }

    public Character SelectedCharacter { get; set; }

    private int deathsInARow = 0;




    // Use this for initialization
    void Start()
    {
        #if UNITY_ANDROID
        Advertisement.Initialize("1753253");
        #endif
        if (FindObjectsOfType<GameManager>().Length > 1)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        string[] boughtChars = GetBoughtCharacters();
        if (boughtChars.Length == 1 && GetBoughtCharacters()[0] == "")
        {
            AddBoughtCharacter("Player01");
        }
    }

    public void AddPoints(int points)
    {
        Points += points;
    }

    public bool BuyCharacter(CharacterAndPrice character)
    {
        if (Points >= character.price)
        {
            Points -= character.price;
            AddBoughtCharacter(character.name);
            return true;
        }
        return false;
    }

    public string[] GetBoughtCharacters()
    {
        string rawString = PlayerPrefs.GetString("Characters");
        return rawString.Split(',');
    }

    public void AddBoughtCharacter(string name)
    {
        List<string> chars = GetBoughtCharacters().ToList();
        chars.Add(name);

        string saveString = "";
        foreach (var characterName in chars)
        {
            saveString += characterName + ",";
        }
        saveString = saveString.Remove(saveString.Length - 1, 1);

        PlayerPrefs.SetString("Characters", saveString);

    }
    [ContextMenu("Reset")]
    public void Reset()
    {
        PlayerPrefs.SetString("Characters", "");
        Points = 0;
    }

    public void Died()
    {
        deathsInARow++;
        if (deathsInARow >= 3)
        {
            ShowAd();
            deathsInARow = 0;
        }
    }
    private void ShowAd()
    {
        #if UNITY_ANDROID
        if (Advertisement.IsReady())
        {
            Advertisement.Show();
        }
        #endif
    }
}
