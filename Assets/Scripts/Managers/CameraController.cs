﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float MoveSpeed = 3f;
    private Player player;

    private Vector3 origZ;
    private bool isShakingCamera = false;

    private void Start()
    {
        player = FindObjectOfType<Player>();
        origZ = new Vector3(0, 0, transform.position.z);
    }

    private void LateUpdate()
    {
        if (isShakingCamera == false)
        {
            transform.position = Vector3.Lerp(transform.position, player.transform.position, MoveSpeed * Time.deltaTime) + origZ;
        }
    }

    public void CameraShake(float strength, float duration, int speed)
    {
        StartCoroutine(ShakeCamera(strength/32f, duration,speed)); //TODO: PIXEL SIZE
    }

    private IEnumerator ShakeCamera(float strength, float duration, int speed)
    {
        float shakeDuration = 0f;
        isShakingCamera = true;
        Vector3 origCamPos = transform.position;

        while (shakeDuration < duration)
        {

            float xShake = (Random.value * 2 - 1) * strength;
            float yShake = (Random.value * 2 - 1) * strength;

            transform.position = origCamPos + new Vector3(xShake, yShake, origZ.z);

            for (int i = 0; i < speed; i++)
            {
                yield return null;
            }

            shakeDuration += Time.deltaTime;
        }
        isShakingCamera = false;
    }
}
