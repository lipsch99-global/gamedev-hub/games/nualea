﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class CharacterSelectionManager : MonoBehaviour
{
    [Range(0.1f, 1f)]
    public float height;
    public CharacterAndPrice[] AllCharacters;
    public Transform CharacterHolder;
    public GameObject CharacterUIPrefab;

    public Text PointsLabel;
    public ScrollRect scrollRect;

    private GameManager gameManager;

    private List<CharacterUI> characterUIs;

    private RectTransform characterHolderRectTransform;

    // Use this for initialization
    void Start()
    {
        if (AllCharacters.Length == 0)
        {
            Debug.LogException(new System.Exception("No Characters assigned"));
            return;
        }

        gameManager = FindObjectOfType<GameManager>();
        characterHolderRectTransform = CharacterHolder.GetComponent<RectTransform>();

        if (gameManager.SelectedCharacter == null)
        {
            gameManager.SelectedCharacter = AllCharacters[0].character;
        }


        ShowAllCharacters();
        ReloadBoughtStatus();
        ReloadPointsView();

    }

    void Update()
    {
        Debug.Log("screen: " + Screen.height);
    }

    private void ShowAllCharacters()
    {
        characterUIs = new List<CharacterUI>();
        characterHolderRectTransform.sizeDelta = new Vector2(Screen.width * 5, Screen.height * 0.7f);
        float pixelHeight = 200;
        float padding = pixelHeight / 6f;
        float relativeXPos = padding + pixelHeight/2f;
        foreach (var character in AllCharacters)
        {
            CharacterUI charUI = Instantiate(CharacterUIPrefab, CharacterHolder).GetComponent<CharacterUI>();
            RectTransform charUIRectTransform = charUI.GetComponent<RectTransform>();

            charUIRectTransform.sizeDelta = new Vector2(pixelHeight, pixelHeight);
            charUIRectTransform.anchoredPosition = new Vector3(relativeXPos, charUIRectTransform.anchoredPosition.y);
            characterUIs.Add(charUI);
            charUI.Character = character;
            charUI.Clicked += CharacterClicked;
            charUI.ShowEverything(pixelHeight);

            charUI.SetSelectedStatus(character.character == gameManager.SelectedCharacter);
            relativeXPos += pixelHeight + padding;
        }
        relativeXPos -= padding;
        characterHolderRectTransform.sizeDelta = new Vector2(relativeXPos, Screen.height - Screen.height * 0.5f);
        scrollRect.horizontalNormalizedPosition = 0f;

    }

    public void BackToMainMenu()
    {
        FindObjectOfType<SceneChanger>().GoToMenuScene();
    }

    public void StartGame()
    {
        FindObjectOfType<SceneChanger>().GoToMainScene();
    }

    private void CharacterClicked(CharacterUI clickedCharacterUI)
    {
        string[] boughtCharacters = gameManager.GetBoughtCharacters();

        if (!boughtCharacters.Contains(clickedCharacterUI.Character.name))
        {
            gameManager.BuyCharacter(clickedCharacterUI.Character);
            ReloadBoughtStatus();
            ReloadPointsView();
        }

        if (gameManager.SelectedCharacter != clickedCharacterUI.Character.character)
        {
            foreach (var characterUI in characterUIs)
            {
                if (characterUI == clickedCharacterUI)
                {
                    if (boughtCharacters.Contains(clickedCharacterUI.Character.name))
                    {
                        gameManager.SelectedCharacter = characterUI.Character.character;
                        continue;
                    }
                }
            }
        }
        RealoadSelectedStatus();
    }

    private void RealoadSelectedStatus()
    {
        foreach (var characterUI in characterUIs)
        {
            if (characterUI.Character.character == gameManager.SelectedCharacter)
            {
                characterUI.SetSelectedStatus(true);
            }
            else
            {
                characterUI.SetSelectedStatus(false);
            }
        }
    }

    private void ReloadBoughtStatus()
    {
        string[] boughtCharacters = gameManager.GetBoughtCharacters();

        foreach (var characterUI in characterUIs)
        {
            if (boughtCharacters.Contains(characterUI.Character.name))
            {
                characterUI.SetBuyStatus(false);
            }
            else
            {
                characterUI.SetBuyStatus(true);
            }
        }
    }

    private void ReloadPointsView()
    {
        PointsLabel.text = gameManager.Points.ToString().PadLeft(3, '0');
    }


}

[System.Serializable]
public struct CharacterAndPrice
{
    public Character character;
    public int price;
    public string name;

}
