﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    public float highVolume;
    public float lowVolume;
    public float transitionTime;

    [Header("Audio Sources")]
    public AudioSource musicSource;
    public AudioSource soundSource;
    [Header("AudioMixer")]
    public AudioMixer audioMixer;
    [Header("Audio Clips")]
    public AudioClip deathSound;
    public AudioClip killSound;

    public bool IsMusicMuted { get; private set; }
    public bool IsSoundMuted { get; private set; }


    // Use this for initialization
    void Start()
    {
        IsMusicMuted = false;
        IsSoundMuted = false;
        musicSource.loop = true;
        musicSource.volume = lowVolume;
        musicSource.Play();
    }

    public void LowerBackgroundMusic()
    {
        StartCoroutine(TransitionVolume(lowVolume));
    }

    public void RaiseBackgroundMusic()
    {
        StartCoroutine(TransitionVolume(highVolume));
    }

    private IEnumerator TransitionVolume(float to)
    {
        float from = musicSource.volume;
        float increasePerSecond = (to - from) / transitionTime;

        while ((to > from && musicSource.volume < to) || (to < from && musicSource.volume > to))
        {
            musicSource.volume += increasePerSecond * Time.deltaTime;
            yield return null;
        }

        musicSource.volume = to;
    }

    public void PlayKillSound()
    {
        soundSource.clip = killSound;
        soundSource.Play();
    }

    public void PlayDeathSound()
    {
        soundSource.clip = deathSound;
        soundSource.Play();
    }

    public void SetMusicMuteState(bool muted)
    {
        audioMixer.SetFloat("MusicVolume", muted ? -80 : 0);
        IsMusicMuted = muted;
    }
    public void SetSoundMuteState(bool muted)
    {
        audioMixer.SetFloat("SoundVolume", muted ? -80 : 0);
        IsSoundMuted = muted;
    }

}
