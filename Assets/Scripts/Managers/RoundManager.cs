﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundManager : MonoBehaviour
{
    public enum Turn { Player, Enemy, GameOver }
    public Turn WhosTurn { get; private set; }

    public Player PlayerObject;
    public System.Action<int> KillAmountChanged;


    private EnemyManager enemySpawner;

    private int _KillAmount = 0;
    private int KillAmount
    {
        get
        {
            return _KillAmount;
        }
        set
        {
            _KillAmount = value;
            if (KillAmountChanged != null)
            {
                KillAmountChanged(_KillAmount);
            }
        }
    }

    private Map map;
    private CameraController camController;
    private UIManager uiManager;
    private SceneChanger sceneChanger;
    private GameManager gameManager;
    private SoundManager soundManager;



    void Start()
    {
        WhosTurn = Turn.Player;
        enemySpawner = FindObjectOfType<EnemyManager>();
        map = FindObjectOfType<Map>();
        camController = FindObjectOfType<CameraController>();
        uiManager = FindObjectOfType<UIManager>();
        sceneChanger = FindObjectOfType<SceneChanger>();
        gameManager = FindObjectOfType<GameManager>();
        soundManager = FindObjectOfType<SoundManager>();

        RoundStart();
    }

    private void RoundStart()
    {
        //Reload Map
        map.ReloadMap();

        //Set Player
        Vector2Int position = Vector2Int.zero; //new Vector2Int(Mathf.FloorToInt(map.Size.x / 2f), Mathf.FloorToInt(map.Size.y / 2f));
        MapTile playerTile = map.GetMapTileByPosition(position);

        PlayerObject.EntitiyCharacter = gameManager.SelectedCharacter;
        PlayerObject.Initialise();
        PlayerObject.Position = position;
        PlayerObject.AlignPosition();
        playerTile.SetEntity(PlayerObject);

        //Spawn Enemies
        enemySpawner.Initialise();
        enemySpawner.RespawnEnemies();
    }

    public void PlayerDidTurn()
    {
        StartCoroutine(TurnEnd());
    }

    public void PlayerStartsMove()
    {
        WhosTurn = Turn.Enemy;
    }

    private IEnumerator TurnEnd()
    {
        yield return new WaitForSeconds(0.3f);

        enemySpawner.NewTurnStarted();

        yield return new WaitForSeconds(0.3f);

        if (WhosTurn != Turn.GameOver)
        {
            WhosTurn = Turn.Player;
        }
    }

    public void EnemyKilled(int coins)
    {
        KillAmount+= coins;
        soundManager.PlayKillSound();
    }

    public void PlayerDied()
    {
        soundManager.PlayDeathSound();
        WhosTurn = Turn.GameOver;
        uiManager.ActivateDeathScreen();
        PlayerObject.GetComponent<SpriteRenderer>().enabled = false;

        GameManager gm = FindObjectOfType<GameManager>();
        gm.AddPoints(KillAmount);
        gm.Died();
    }

    public void Replay()
    {
        sceneChanger.GoToMainScene();
    }

    public void LoadMenu()
    {
        sceneChanger.GoToMenuScene();
    }

    public void ShowCharacterSelection()
    {
        sceneChanger.GoToCharacterSelection();
    }
}
