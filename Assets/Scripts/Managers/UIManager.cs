﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text KillCount;

    public Canvas gameCanvas;

    public Canvas deathScreenCanvas;

    public Text DeathScreenKillCount;

    private RoundManager roundManager;
    // Use this for initialization
    void Start()
    {
        roundManager = FindObjectOfType<RoundManager>();
        roundManager.KillAmountChanged += UpdateKillAmount;
        UpdateKillAmount(0);
        deathScreenCanvas.gameObject.SetActive(false);
    }

    public void ActivateDeathScreen()
    {
        DeathScreenKillCount.text = KillCount.text;

        gameCanvas.gameObject.SetActive(false);
        deathScreenCanvas.gameObject.SetActive(true);
    }



    private void UpdateKillAmount(int amount)
    {
        KillCount.text = amount.ToString().PadLeft(2, '0');
    }
}
